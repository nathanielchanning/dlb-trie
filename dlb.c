/*
  The MIT License (MIT)

Copyright (c) 2016 Nathaniel Channing

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include "dlb.h"

struct ll_node * ll_start = NULL;
struct dlb_node * dlb_start = NULL;

/* The linked list exists solely to speed
   up the development process by not having to
   write a function to traverse and free the trie. */

/* Adds an address to the linked list. */
void ll_add (void * to_add_address) {
    struct ll_node * malloc_val = (struct ll_node *) malloc (sizeof (struct ll_node));

    malloc_val-> address = to_add_address;
    malloc_val-> next = ll_start;
    ll_start = malloc_val;
}

/* Frees ll_start.
   Assumes the address field of ll_node is a malloc'd value. */
void ll_free () {
    struct ll_node * curr = ll_start;
    struct ll_node * temp; 

    while (curr != NULL) {
        temp = curr;
        curr = curr-> next;
        free (temp-> address);
        free (temp);
    }
}

/* Creates a node for the dlb and adds it
   to the linked list. */
struct dlb_node * dlb_create_node (unsigned int letter) {
    struct dlb_node * node = (struct dlb_node *) malloc (sizeof (struct dlb_node *));
    ll_add ((void *) node);

    node-> val =  letter;
    node-> other = NULL;
    node-> next = NULL;
    return node;
}

/* Search the dlb to see if a value exists.
   Returns the index, or -1 if it doesn't exist. */
int dlb_search (char * str) {
    struct dlb_node * curr = dlb_start;
    int i = 0;

    /* loop through to find match. */
    while (1) {
        /* Check if node is NULL */
        if (curr == NULL) {
            return -1;

        /* If current value matches, move to
           next level. Or check if it is the end, and
           return. */
        } else if ((char) curr-> val == str [i]) {

            /* Return the index. */
            if (str [i++] == '\0') {
                return curr-> next-> val;
            }

            /* or get the next level. */
            curr = curr-> next;

        /* Else check the other vals on the level. */
        } else {

            curr = curr-> other;
        }
    }

}

/* Adds a string to the dlb
   pos will be the index in the string
   table. Think of this as a mapping of
   a string -> unsigned int. */
void dlb_add (char * str, unsigned int pos) {
    struct dlb_node * curr = dlb_start;
    int i = 0;


    /* Check if the dlb is empty. */
    if (curr != NULL) {
        
        /* Check for similar characters. */
        while (1) {
            /* Check if the current char matches. */
            if ((char) curr-> val == str [i]) {
                i++;

                /* move to the next level. */
                curr = curr-> next;

            /* Create the new node and go from there. */
            } else if (curr-> other == NULL) {
                curr-> other = dlb_create_node ((unsigned int) str [i]);
                curr = curr-> other;

                /* check if that was the end of it.
                   Adding the pos to next if so. */
                if (str [i++] == '\0') {
                    curr-> next = dlb_create_node (pos);

                    return;
                }
                
                break;

            /* else grab other node */
            } else {
                curr = curr-> other;
            }
        }

    /* if empty, just add the first node. */
    } else {
        dlb_start = dlb_create_node (str [i++]);  
        curr = dlb_start;
    }

    /* Finish adding the string to the trie. */
    while (1) {
        curr-> next = dlb_create_node (str [i]);
        curr = curr-> next;

        /* check if the string is done. */
        if (str [i++] == '\0') {
            curr-> next = dlb_create_node (pos);

            break;
        }
    }
    
}
