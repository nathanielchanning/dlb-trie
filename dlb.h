/*
  The MIT License (MIT)

Copyright (c) 2016 Nathaniel Channing

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
   
#include <stdio.h>
#include <stdlib.h>

struct dlb_node {
    /* The val isn't what we are looking for.
       An example, would be if we're looking for 'the'
       val could be 'x', so we would continue on 
       through other to find 'h'. */
    struct dlb_node * other; 

    unsigned int val;

    /* The next node if val is what we're looking for.
       An example, would be if we're looking for 'the'
       val would be 'h', so we would continue on 
       through next to find 'e'. */
    struct dlb_node * next; 
};

/* Used to keep track of the nodes that will need
   to be freed at the end of the program. */
struct ll_node {
    void * address;
    struct ll_node * next;
};

/* Called at the end to free memory.
   Always free memory and files.
   'lsof | grep' is a great way to find
   leak resources, though this isn't
   all that relevant here. */
void ll_free (); 

/* searches dlb for string. Keep in mind
   that dlb_add is adding an UNSIGNED INT, but
   this returns an int. This is because our actual
   program is going to be capped far below the maximum
   positive value of an int. */
int dlb_search (char * str); 

/* adds string to dlb that maps to pos, i.e.,
 this function maps a char* -> unsigned int */
void dlb_add (char * str, unsigned int pos); 
